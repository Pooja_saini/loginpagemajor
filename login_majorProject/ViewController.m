//
//  ViewController.m
//  login_majorProject
//
//  Created by Click Labs133 on 12/8/15.
//  Copyright (c) 2015 Clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{UIView *view1;}
@property (strong, nonatomic) IBOutlet UIButton *clickForLoginButton;

@end

@implementation ViewController
@synthesize clickForLoginButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    view1=[[UIView alloc]init];
    view1.frame=CGRectMake(-300, 170, 300, 400);
    view1.backgroundColor=[UIColor blueColor];
    [self.view addSubview:view1];
}
- (IBAction)clickForLoginButtonAction:(id)sender {
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         view1.frame=CGRectMake(30, 170, 300, 400);
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
