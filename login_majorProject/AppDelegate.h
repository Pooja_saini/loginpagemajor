//
//  AppDelegate.h
//  login_majorProject
//
//  Created by Click Labs133 on 12/8/15.
//  Copyright (c) 2015 Clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

