//
//  main.m
//  login_majorProject
//
//  Created by Click Labs133 on 12/8/15.
//  Copyright (c) 2015 Clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
